#!/usr/bin/perl -w
use v5.14;
use strict;
use warnings;

use Gruntmaster::Daemon qw/process/;

use Cwd qw/getcwd/;
use File::Slurp qw/read_file/;
use File::Temp qw/tempdir/;
use HTTP::Tiny;
use Hash::Merge qw/merge/;
use JSON qw/encode_json decode_json/;
use Log::Log4perl;
use Test::HTTP::LocalServer;
use Test::More tests => 3;
use YAML::Tiny qw/LoadFile/;

my $server = Test::HTTP::LocalServer->spawn;
my $response = HTTP::Tiny->new->get( $server->url );
$ENV{REMOTE_ADDRESS} = $server->url;

my $loglevel = $ENV{TEST_LOG_LEVEL} // ($ENV{TEST_VERBOSE} ? 'TRACE' : 'OFF');
my $log_conf = <<CONF;
log4perl.category.Gruntmaster.Daemon = $loglevel, stderr

log4perl.appender.stderr                          = Log::Log4perl::Appender::Screen
log4perl.appender.stderr.layout                   = Log::Log4perl::Layout::PatternLayout
log4perl.appender.stderr.layout.ConversionPattern = [\%d] [\%F{1}:\%M{1}:\%L] [\%p] \%m\%n
CONF
Log::Log4perl->init(\$log_conf);

$ENV{PATH} = getcwd . ':' . $ENV{PATH};
my $tempdir = tempdir "gruntmasterd-testingXXXX", TMPDIR => 1, CLEANUP => 1;
chmod 0777, $tempdir;

sub process_meta() {
  my $pbmeta = LoadFile "t/problems/aplusb/meta.yml";
  for (1 .. $pbmeta->{testcnt}) {
		$pbmeta->{infile}[$_ - 1] = read_file "t/problems/aplusb/$_.in"
      if $pbmeta->{generator} eq 'File';
		$pbmeta->{okfile}[$_ - 1] = read_file "t/problems/aplusb/$_.ok"
      if $pbmeta->{runner} && $pbmeta->{runner} eq 'File';
  }
  my $source = <t/problems/aplusb/tests/40>;
  my $meta = LoadFile "$source/meta.yml";
  $meta->{files}{prog}{content} = read_file "$source/$meta->{files}{prog}{name}";
  $meta = merge $meta, $pbmeta;
  my $savedcwd = getcwd;
  chdir $tempdir;
  $meta->{job_id} = 1;
  Gruntmaster::Daemon::process($meta);
  chdir $savedcwd;
}

sub send_request() {
  process_meta();

  my $requests = $server->get_log;
  is $requests =~ qr/"result_text":"40 points"/, 1;
  is $requests =~ qr/"result":10/, 1;
  is $requests =~ qr/"gm_id":1/, 1;
}

send_request();

$server->stop;
