use v5.14;
use strict;
use warnings;

use FindBin;

use Test::More tests => 1;
BEGIN {
  $ENV{PATH} = "$FindBin::Bin/../blib/script:" . $ENV{PATH};
  use_ok('Gruntmaster::Daemon')
};
