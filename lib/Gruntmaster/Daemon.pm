package Gruntmaster::Daemon;

use 5.014000;
use strict;
use warnings;

our $VERSION = '5999.000_005';

use Gruntmaster::Daemon::Constants qw/ERR/;
use Gruntmaster::Daemon::Format qw/prepare_files stopvms/;
use Gruntmaster::SendResults qw/send_results_request/;

use File::Slurp qw/read_file/;
use File::Temp qw/tempdir/;
use JSON qw/decode_json encode_json/;
use Sys::Hostname qw/hostname/;
use Try::Tiny;
use Log::Log4perl qw/get_logger/;

use constant FORMAT_EXTENSION => {
	BRAINFUCK => 'bf',
	C => 'c',
	CPP => 'cpp',
	D => 'd',
	GCCGO => 'go',
	GOLANG => 'go',
	GOLFSCRIPT => 'gs',
	HASKELL => 'hs',
	JAVA => 'java',
	JAVASCRIPT => 'js',
	JULIA => 'jl',
	MONO => 'cs',
	OBERON => 'm',
	OCAML => 'ml',
	PASCAL => 'pas',
	PERL => 'pl',
	PHP => 'php',
	PYTHON => 'py',
	PYTHON3 => 'py',
	RUBY => 'rb',
	RUST => 'rs',
	SBCL => 'l',
};

##################################################

sub safe_can {
	my ($type, $sub, $name) = @_;

	get_logger->logdie("Invalid \l$type: '$name'") unless $name =~ /^\w+$/s;
	my $pkg = __PACKAGE__ . "::${type}::${name}";
	eval "require $pkg" or get_logger->warn("Error while requiring $pkg: $@");
	$pkg->can($sub) or get_logger->logdie("No such \l$type: '$name'");
}

sub process{
	my ($meta) = @_;

	my @results;
	my @full_results = ();
	our $errors = '';
	try {
		prepare_files $meta;
		chomp $errors;

		my ($files, $generator, $runner, $judge, $testcnt) = map { $meta->{$_} or die "Required parameter missing: $_\n"} qw/files generator runner judge testcnt/;

		$generator = safe_can Generator => generate => $generator;
		$runner = safe_can Runner => run => $runner;
		$judge = safe_can Judge => judge => $judge;

		for my $test (1 .. $testcnt) {
			my $result;
			unlink 'result';
			try {
				$generator->($test, $meta);
				$result = $runner->($test, $meta);
			} catch {
				$result = $_;
				unless (ref $result) {
					chomp $result;
					$result = [ERR, $result];
				}
			};

			my $time = '?';
			$time = (read_file 'time-stop-prog') - (read_file 'time-start-prog') if -f 'time-stop-prog' && -f 'time-start-prog';
			unlink 'time-start-prog', 'time-stop-prog';
			if (ref $result) {
				my $result_text = $result->[1];
				$result_text .= ': ' . read_file 'result' if -s 'result';
				get_logger->trace("Test $test result is " . $result_text);
				push @full_results, {id => $test, result => $result->[0], result_text => $result_text, time => $time}
			} else {
				get_logger->trace("Test $test result is $result");
				push @full_results, {id => $test, result => 0, result_text => $result, time => $time}
			}
			push @results, $result;
			last if $meta->{judge} eq 'Absolute' && ref $result
		}

		my %results = $judge->(@results);
		$meta->{$_} = $results{$_} for keys %results;
		$meta->{results} = \@full_results
	} catch {
		s/(.*) at .*/$1/s;
		chomp;
		$meta->{result} = -1;
		$meta->{result_text} = $_;
	};
	stopvms;
	$meta->{errors} = $errors;

	get_logger->info('Job result: ' . $meta->{result_text});
	send_results_request($meta->{job_id}, $meta->{result}, $meta->{result_text});
}

sub process_job {
	my ($job) = @_;
	my $pb = db()->select('problems', '*', {id => $job->{problem}})->hash;
	my $meta = {
		problem => $job->{problem},
		files => {
			prog => {
				name => 'prog.' . $job->{extension},
				format => $job->{format},
				content => $job->{source},
			},
		},
		map { $_ => $pb->{$_} } qw/generator runner judge precnt testcnt timeout olimit/
	};
	my $timeout_override = db()->query('SELECT timeout FROM limits WHERE problem=? AND format=?', $job->{problem}, $job->{format})->list;
	$meta->{timeout} = $timeout_override if defined $timeout_override;
	$meta->{tests} = decode_json $pb->{tests} if $meta->{runner} eq 'File';
	my $contest = $job->{contest} && contest_entry($job->{contest});
	delete $meta->{precnt} unless $contest && $contest->{started} && !$contest->{finished}; ## no critic (ProhibitNegativeExpressionsInUnlessAndUntilConditions)
	$meta->{testcnt} = $meta->{precnt} if $meta->{precnt};

	$meta->{files}{ver} = {
		name => 'ver.' . FORMAT_EXTENSION->{$pb->{verformat}},
		format => $pb->{verformat},
		content => $pb->{versource},
	} if $pb->{verformat};
	$meta->{job_id} = $job->{id};

	process $meta;

	$meta->{result_text} .= ' (pretests)' if $meta->{precnt};
	finish_job($job, $job->{private} || $pb->{private},
	  result => $meta->{result},
	  result_text => $meta->{result_text},
	  ($meta->{results} ? (results => encode_json $meta->{results}) : ()),
	  $meta->{errors} ? (errors => $meta->{errors}) : ());
}

sub got_job{
	my $job = $_[0];
	my $id = $job->{id};
	get_logger->debug("Processing job $id...");
	process_job $job;
	get_logger->debug("Job $id done");
}

my $daemon = hostname . ":$$";

sub run{
	require Gruntmaster::Data;
	Gruntmaster::Data->import;
	dbinit($ENV{GRUNTMASTER_DSN} // 'dbi:Pg:');
	Log::Log4perl->init('/etc/gruntmasterd/gruntmasterd-log.conf');
	get_logger->info("gruntmasterd $VERSION started ($daemon)");
	my $dir = tempdir 'gruntmasterd.XXXX', CLEANUP => 1, TMPDIR => 1;
	chmod 0711, $dir;
	chdir $dir;
	while (1) {
		my $job = take_job($daemon);
		got_job $job if defined $job;
		sleep 2 unless defined $job;
	}
}

1;
__END__

=head1 NAME

Gruntmaster::Daemon - Gruntmaster 6000 Online Judge -- daemon

=head1 SYNOPSIS

  use Gruntmaster::Daemon;
  Gruntmaster::Daemon->run;

=head1 DESCRIPTION

Gruntmaster::Daemon is the daemon component of the Gruntmaster 6000 online judge.

=head1 AUTHOR

Marius Gavrilescu E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2014 by Marius Gavrilescu

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


=cut
