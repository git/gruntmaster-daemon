package Gruntmaster::Daemon::Format;

use 5.014000;
use strict;
use warnings;
use parent qw/Exporter/;
no if $] > 5.017011, warnings => 'experimental::smartmatch';

use Digest::SHA qw/sha256_hex/;
use Expect;
use File::Basename qw/fileparse/;
use File::Copy qw/cp/;
use File::Slurp qw/read_file write_file/;
use List::MoreUtils qw/natatime/;
use Log::Log4perl qw/get_logger/;
use String::ShellQuote qw/shell_quote/;
use Time::HiRes qw/time/;
use Try::Tiny;

our $VERSION = '5999.000_005';
our @EXPORT_OK = qw/prepare_files stopvms/;

##################################################

our (%vm, %pid);

sub runvm {
	my ($name, $arg) = @_;
	return unless $ENV{GRUNTMASTER_VM};
	my $cmd = $ENV{GRUNTMASTER_VM};
	$cmd .= ' ' . $arg if $arg;
	get_logger->trace("Starting VM $name ($cmd)");
	$vm{$name} = Expect->new;
	$vm{$name}->raw_pty(1);
	$vm{$name}->log_stdout(0);
	$vm{$name}->spawn($cmd);
	$vm{$name}->expect(5, '# ') or get_logger->logdie("Error while starting VM $name: ". $vm{$name}->error);
}

sub stopvms {
	kill KILL => $_->pid for values %vm;
	%vm = %pid = ();
}

sub execlist_finish {
	my ($vm, $kill) = @_;

	if ($vm{$vm}) {
		warn "Cannot kill VM\n" if $kill;
		$vm{$vm}->expect(5, '# ');
	} else {
		kill KILL => $pid{$vm} if $kill;
		waitpid $pid{$vm}, 0;
	}
	write_file "time-stop-$vm", time;
	return if $kill;

	my $er = "exec-result-$vm";
	die "gruntmaster-exec died\n" if -z $er;
	my ($excode, $exmsg) = read_file $er;
	unlink $er;
	chomp ($excode, $exmsg); ## no critic (ProhibitParensWithBuiltins)
	get_logger->trace("Exec result from $vm: $excode $exmsg");
	die [$excode, $exmsg] if $excode; ## no critic (RequireCarping)
}

sub execlist {
	my ($vm, @args) = @_;
	write_file "time-start-$vm", time;
	my $er = "exec-result-$vm";
	if ($vm{$vm}) {
		my $cmd = ">$er " . shell_quote 'gruntmaster-exec', @args;
		get_logger->trace("Running in VM $vm: $cmd");
		$vm{$vm}->send($cmd, "\n");
	} else {
		$pid{$vm} = fork // die "Cannot fork\n";
		unless ($pid{$vm}) {
			open STDOUT, '>', $er or die "Cannot open $er\n";
			get_logger->trace("Running: gruntmaster-exec @args");
			exec 'gruntmaster-exec', @args;
		}
	}
}

sub mkrun{
	my $format = shift;
	sub{
		local *__ANON__ = 'mkrun_runner';
		my ($name, %args) = @_;
		get_logger->trace("Running $name...");
		my $basename = fileparse $name, qr/[.][^.]*/s;
		my @args = ('--sudo');
		push @args, '--keep-stderr' if $ENV{TEST_VERBOSE};
		push @args, '--timeout', $args{timeout} if $args{timeout};
		push @args, '--mlimit',  $args{mlimit}  if $args{mlimit};
		push @args, '--olimit',  $args{olimit}  if $args{olimit};
		my @fds = exists $args{fds} ? @{$args{fds}} : ();
		my $it = natatime 2, @fds;
		while (my ($fd, $file) = $it->()) {
			push @args, "--fd=$fd $file";
		}
		execlist $basename, @args, '--', "./$basename", @{$args{args}};
		execlist_finish $basename unless $args{nonblocking}
	}
}

sub prepare{
	my ($name, $format) = @_;
	get_logger->trace("Preparing file $name...");

	try {
		execlist prog => '--fd=1 >>errors', '--fd=2 >>errors', 'gruntmaster-compile', $format, $name;
		execlist_finish 'prog';
	} catch {
		my $exmsg = $_->[1];
		die "Compile error ($exmsg)\n"
	} finally {
		$Gruntmaster::Daemon::errors .= read_file 'errors';
		$Gruntmaster::Daemon::errors .= "\n" if -s 'errors';
		unlink 'errors';
	};
}

sub prepare_files{
	my $meta = shift;
	if ($meta->{runner} eq 'Interactive') {
		runvm ver => '-serial unix:vm.sock,nowait,server';
		runvm prog => '-serial unix:vm.sock,nowait';
	} else {
		runvm $_ for keys %{$meta->{files}};
	}

	for my $file (values %{$meta->{files}}) {
		my ($format, $name, $content) = @{$file}{qw/format name content/};

		$file->{run} = mkrun($format);
		write_file $name, $content;
		if ($ENV{GRUNTMASTER_CCACHE}) {
			my $key = lc sha256_hex($content) . '-' . $format;
			my $cachefn = "$ENV{GRUNTMASTER_CCACHE}/$key";
			my $exefn = fileparse $name, qr/[.][^.]*/s;
			if (cp $cachefn, $exefn) {
				get_logger->trace("File $name found in compilation cache")
			} else {
				prepare $name, $format;
				cp $exefn, $cachefn
			}
		} else {
			prepare $name, $format
		}
	}
}

1;
__END__

=encoding utf-8

=head1 NAME

Gruntmaster::Daemon::Format - Utility functions for handling source files

=head1 SYNOPSIS

  use Gruntmaster::Daemon::Format qw/prepare_files/;
  prepare_files { files => {
    prog => {
      name => 'prog.pl',
      format => 'PERL',
      content => 'print "Hello, world!"'
    },
    ver => {
      name => 'ver.cpp',
      format => 'CPP',
      content => ...
    },
  }};

=head1 DESCRIPTION

Gruntmaster::Daemon::Format exports utility functions for handling source files.

=over

=item B<prepare_files> I<$meta>

Compiles all the source files in C<< $meta->{files} >>.

=back

=head1 AUTHOR

Marius Gavrilescu E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2014 by Marius Gavrilescu

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


=cut
