package Gruntmaster::SendResults;

use 5.014000;
use strict;
use warnings;
use parent qw/Exporter/;

use HTTP::Request;
use JSON qw/encode_json decode_json/;
use LWP::UserAgent;

our $VERSION = '5999.000_005';
our @EXPORT_OK = qw/send_results_request/;

sub send_results_request {
  my ($job_id, $result, $result_text) = @_;

  my $url = $ENV{REMOTE_ADDRESS};
  my $header = [
    'Content-Type' => 'application/json'
  ];
  my $data = {
    gm_id => $job_id,
    result => $result,
    result_text => $result_text
  };

  my $encoded_data = encode_json($data);
  my $request = HTTP::Request->new(POST => $url, $header, $encoded_data);
  my $ua = LWP::UserAgent->new();
  my $response = $ua->request($request);

  return $response;
}

1;
